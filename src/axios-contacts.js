import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://burger-54e1f.firebaseio.com/'
})

export default instance;
