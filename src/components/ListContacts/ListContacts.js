import React from 'react';
import Contact from './Contact/Contact'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Contacts';


const useStyles = makeStyles(theme => ({
  input: {
    padding: theme.spacing(2),
    '&:hover': {
      background: "#cbcbcb",
    }
  },
}));

const ListContacts = props => {
  const classes = useStyles();

  let names = []
  for (let key in props.contacts) {
    names.push(props.contacts[key].nombre)
  }

  names.map(c =>  {
    return (
      <Contact name={c}/>
    )
  })

  return (
    <>
      <Button
        variant="contained"
        size="large"
        fullWidth
        onClick={props.create}
        color="primary">
        <CreateIcon />
        Nuevo Contacto
      </Button>
      {names.map((e, i) => {
        return (
          <div
            onClick={() => props.showContact(i)}
            className={classes.input}>
            {e}
          </div>
        )
      })}
    </>
  )
};

export default ListContacts;
