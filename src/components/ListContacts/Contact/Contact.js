import React from 'react';
import Typography from '@material-ui/core/Typography';

const Contact = props => (
  <Typography variant="h6" gutterBottom>
    {props.name}
  </Typography>
);

export default Contact;
