import React, { Component } from 'react'
import axios from '../../axios-contacts'
import Input from '../Input/Input'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

class NewContact extends Component {
  state = {
    contactForm: {
      nombre: {
        label: 'Nombre',
        type: 'input',
        value: ''
      },
      paterno: {
        label: 'Apellido Paterno',
        type: 'input',
        value: ''
      },
      materno: {
        label: 'Apellido Materno',
        type: 'input',
        value: ''
      },
      numeroCelular: {
        label: 'Celular',
        type: 'input',
        value: ''
      },
      numeroCasa: {
        label: 'Casa',
        type: 'input',
        value: ''
      },
      direccion: {
        label: 'Direccion',
        type: 'input',
        value: ''
      },
      nota: {
        label: 'Nota',
        type: 'input',
        value: ''
      }
    }
  }

  newContact = e => {
    e.preventDefault()
    const form = this.state.contactForm
    const newContact = {
      nombre: form.nombre.value,
      paterno: form.paterno.value,
      materno: form.materno.value,
      numeroCelular: form.numeroCelular.value,
      numeroCasa: form.numeroCasa.value,
      direccion: form.direccion.value,
      nota: form.nota.value,
    }

    axios.post('/contacts.json', newContact)
      .then(response => {
        console.log(response)
        window.location.reload()
      }).catch(error => {
        console.log(error)
      })
  }

  updateForm = (e, id) => {
    const updateContact = {
      ...this.state.contactForm
    }
    const updateElement = {
      ...updateContact[id]
    }
    updateElement.value = e.target.value

    updateContact[id] = updateElement
    // this.props.value = e.target.value
    this.setState({contactForm: updateContact})
  }

  render() {
    const form = this.state.contactForm;
    const formArray = [];
    for( let key in form ){
      formArray.push({
        id: key,
        ...form[key]
      });
    }

    return (
      <form onSubmit={this.newContact}>
        {formArray.map(element => (
          <Input
            type={ element.type }
            key={ element.id }
            value={ element.value }
            changed={ ( event ) =>  this.updateForm( event, element.id ) }
            label={ element.label }/>
        ))}
        <Grid item xs={12}>
          <Button onClick={this.newContact} variant="contained" color="primary">
            Crear el contacto
          </Button>
        </Grid>
      </form>
    )
  }
}

export default NewContact;
