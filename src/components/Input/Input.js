import React from 'react'
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';
import InputLabel from '@material-ui/core/InputLabel';
import MateralInput from '@material-ui/core/Input'
import TextField from '@material-ui/core/TextField';

const Input = (props) => {

  const TextMaskCustom = () => {
  const { inputRef, ...other } = props;
    return props.label == 'Celular' ? <MaskedInput
      {...other}
        mask={[ /[1-9]/, /\d/, /\d/,
          ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
        showMask
      />
      :  <MaskedInput
        {...props}
        mask={[ /[1-9]/, /\d/, /\d/,
          ' ', /\d/, /\d/, /\d/, /\d/,]}
        showMask
      />
  }

    let input = ''
    switch(props.type) {
      case 'input':
        input = <TextField
          name="nombre"
          label={props.label}
          value={props.value}
          onChange={props.changed}
          margin="normal"
        />
          break
      case 'numbers':
        // input = TextMaskCustom()
        input =  <MateralInput
        defaultValue="Hello world"
        inputComponent={TextMaskCustom}
      />
        // input = (
        //   <>
        //     <InputLabel htmlFor="formatted-text-mask-input">{props.label}</InputLabel>
        //     <TextField>
        //       <MateralInput
        //         value={props.value}
        //         id="formatted-text-mask-input"
        //         inputComponent={TextMaskCustom}
        //       />
        //   </TextField>
        //   </>
        // )
          break
      case 'textArea':
        input = <TextField
          id="standard-multiline-static"
          label="Nota adicional"
          multiline
          rows="4"
          defaultValue="Es bien chid@"
          margin="normal"
        />
          break
      default:
        input = <input type="text"/>
    }


    return (
      <div>
        {input}
      </div>
    )
}

export default Input;
