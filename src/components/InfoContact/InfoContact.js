import React from 'react';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const InfoContact = props => {

  let info = <p>{props.showContact}</p>
  let element = 0
  for (let key in props.contacts) {
    if(element === props.showContact) {
      info = (
        <>
          <p>
            {props.contacts[key].nombre}
          </p>
          <p>
            {props.contacts[key].paterno}
          </p>
          <p>
            {props.contacts[key].materno}
          </p>
            {props.contacts[key].numeroCelular}
          <p>
          </p>
          <p>
            {props.contacts[key].numeroCasa}
          </p>
          <p>
            {props.contacts[key].direccion}
          </p>
          <p>
            {props.contacts[key].nota}
          </p>
        </>
      )
      break
    }
    element += 1
  }

  return (
    <div>
      {info}
      <Button
        variant="contained"
        size="large"
        onClick={props.editar}
        color="primary">
        <EditIcon />
        Editar
      </Button>
      <div>
        <Button
          onClick={props.deleteContact}
          variant="contained"
          size="large"
          color="secondary">
          <DeleteIcon  />
          Eliminar
        </Button>
      </div>
    </div>
  )
};

export default InfoContact;
