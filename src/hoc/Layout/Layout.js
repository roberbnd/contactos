import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import Header from '../../components/Header/Header'
import Contacts from '../../containers/Contacts/Contacts'

class Layout extends Component {
  render() {
    return (
      <>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Header />
          </Grid>
          <Contacts />
        </Grid>
      </>
    )
  }
}

export default Layout;
