import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import InfoContact from '../../../src/components/InfoContact/InfoContact'
import NewContact from '../../components/NewContact/NewContact'
import UpdateContact from '../../components/UpdateContact/UpdateContact'
import ListContact from '../../../src/components/ListContacts/ListContacts'
import axios from '../../axios-contacts'

class Contacts extends Component {
  state = {
    contacts: [],
    newContact: false,
    showContact: 0,
    updateContactKey: null,
    updateContact: false
  }

  componentDidMount() {
    axios.get('/contacts.json')
      .then(res => {
        const contacts = res.data
        this.setState({contacts: contacts})
      })
  }

  createContact = () => {
    this.setState({updateContact: false})
    this.setState({newContact:true})
  }

  showContact = (id) => {
    this.setState({newContact: false})
    this.setState({updateContact: false})
    this.setState({showContact: id})
  }

  deleteContact = () => {
    let element = 0
    let contact = null
    for (let key in this.state.contacts) {
      if(element === this.state.showContact) {
        contact = key
        break
      }
      element += 1
    }
    console.log(contact)
    axios.delete('/contacts/' + contact + '.json' )
      .then(res => {
        console.log('Eliminado')
        console.log(res)
        window.location.reload()
      }).catch(err  => {
        console.log('No se pudo eliminar')
        console.log(err)
      })
  }

  updateContact = id => {
    let element = 0
    let contact = null
    for (let key in this.state.contacts) {
      if(element === this.state.showContact) {
        contact = key
        break
      }
      element += 1
    }
    this.setState({updateContactKey: contact})
    this.setState({updateContact: true})
  }

  render() {
    return (
      <>
        <Grid item xs={3}>
          <ListContact
            showContact={this.showContact}
            create={this.createContact}
            contacts={this.state.contacts}/>
        </Grid>
        <Grid item xs={9}>
          { this.state.newContact ? <NewContact /> :
          this.state.updateContact ? <UpdateContact
            contacts={this.state.contacts}
            contactKey={this.state.updateContactKey}
          />:
        this.state.contacts !== null ?
          <InfoContact
            editar={this.updateContact}
            deleteContact={this.deleteContact}
            showContact={this.state.showContact}
            contacts={this.state.contacts}/> : 
          <p>Agregar contactos</p>
          }
        </Grid>
     </>
    )
  }
}

export default Contacts;
